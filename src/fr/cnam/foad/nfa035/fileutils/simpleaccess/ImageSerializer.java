package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;

public interface ImageSerializer {

	String serialize(File image);

	byte[] deserialize(String encodedImage);

}
