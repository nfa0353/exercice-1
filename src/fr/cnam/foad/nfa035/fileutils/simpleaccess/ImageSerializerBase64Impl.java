package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.awt.image.BufferedImage;

public class ImageSerializerBase64Impl implements ImageSerializer {
	/**
	 * La method serialize convertit l'image donnée en paramètre en tableau de byte
	 *  puis en chaîne de caractère
	 */

	@Override
	public String serialize(File image) {
		String imageString = null;
		try {
			BufferedImage bImage = ImageIO.read(image);
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			ImageIO.write(bImage, "png", output);
			byte[] fileBytes = output.toByteArray();
			imageString = Base64.getEncoder().encodeToString(fileBytes);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return imageString;
		
	}
	/**
	 * La méthode deserialize décode la chaîne de caractère donnée en paramètre
	 * en tableau de byte
	 */

	@Override
	public byte[] deserialize(String encodedImage) {
		byte[] fileBytes = Base64.getDecoder().decode(encodedImage);
		return fileBytes;		
		
		
	}

}
